export default function ctrlMenuOffCanvas(){
    const doc = document;
    doc.querySelector('.main-header__barMenu').addEventListener('click', showMenu);
}

function showMenu(e){   
    const doc = document;  
    if (e.target.classList.contains('icon-menu')) {
        doc.querySelector('.main-header__menu-container').classList.toggle('active');
    }
}