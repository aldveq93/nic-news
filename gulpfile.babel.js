// Instalación de dependencias
import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import babel from 'gulp-babel';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import minifycss from 'gulp-minify-css';
import pug from 'gulp-pug';
import browserify from 'gulp-browserify';
import streamify from 'gulp-streamify';
const browserSync = require('browser-sync').create();

// Tarea para compilar archivos sass a css con autoprefijos, concatenados y minificados.       
gulp.task('sass', () => {
    gulp.src('./dev/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            versions:['last 2 browsers']
        }))
        .pipe(concat('style.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('./public/css/'))
        .pipe(browserSync.stream());
});


// Tarea para convertir es6 a es5

gulp.task('es6', () =>{
    gulp.src('./dev/es6/*.js')
        .pipe(browserify({
            transform: ['babelify']
        }))
        .pipe(babel())
        .pipe(streamify(uglify())) 
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./public/js/'))
        .pipe(browserSync.stream());
});

// Tareas para compilar archivos pug a html5

gulp.task('pug', () =>{
    gulp.src('./dev/pug/views/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest('./public/views/'))
        .pipe(browserSync.stream());
})

// Tarea para recargar el servidor local ante un cambio en los archivos sass, js ó html5
gulp.task('reloadServer', ['sass','es6','pug'], () => {
    browserSync.init({
        server: {
            baseDir: ["./public/","./public/views/"]
        }
    });
    gulp.watch(['./dev/scss/**/*.scss'], ['sass']);
    gulp.watch(['./dev/es6/**/*.js'], ['es6']);
    gulp.watch(['./dev/pug/**/*.pug'], ['pug']);
    gulp.watch("./public/views/*.html").on('change', browserSync.reload);
});

// Tarea por defecto para ejecutar los demás tareas definidas
gulp.task('default', ['reloadServer']);